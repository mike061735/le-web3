<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div><b>姓名:</b> {{ $name }} {{ $title }}</div>
        <div><b>電子郵件:</b> {{ $email }}</div>
        <div><b>聯絡電話:</b> {{ $tel }}</div>
        <div><b>意見或需求描述:</b></div>
        <div><pre>{{ $comment }}</pre></div>
    </body>
</html>