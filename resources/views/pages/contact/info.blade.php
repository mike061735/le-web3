@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
      <h2 class="card-title fw-bold">聯絡我們</h2>
      <div class="col col-md-4">
        <ul class="mx-5">
            <li>
                <a class="nav-link link-dark" href="/service">需求服務</a>
            </li>
            <li>
                <a class="nav-link link-dark" href="/info">公司資訊</a>
            </li>
        </ul>
      </div>
      <div class="col col-md-8">
        <h3 class="card-title fw-bold text-danger">雲林總公司</h3>
        <div class="row">
            <div class="col-md-5">
                <img src="{{ asset('assets/Map.png') }}" alt="">
            </div>
            <div class="col-md-7 d-flex align-items-center">
                <p class="align-middle text-muted">
                    電話：05-699-7095<br><br>
                    電子郵件 : baojie588@gmail.com<br><br>
                    地址：63543雲林縣東勢鄉東勢西路392巷3號
                </p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    var app = new Vue({
    el: '#app',
    data: {
        message: ''
    }
    })
</script>
@endsection