@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
      <h2 class="card-title fw-bold">聯絡我們</h2>
      <div class="col col-md-4">
        <ul class="mx-5">
            <li>
                <a class="nav-link link-dark" href="/service">需求服務</a>
            </li>
            <li>
                <a class="nav-link link-dark" href="/info">公司資訊</a>
            </li>
        </ul>
      </div>
      <div class="col col-md-8">
        <h3 class="card-title fw-bold text-danger">需求服務</h3>
        <div>
            <div class="row align-items-center my-2 odd">
                <div class="col col-md-2">
                    <label for="name" class="col-form-label">姓名</label>
                </div>
                <div class="col col-md-8">
                    <input type="text" id="name" class="form-control" required v-model="name">
                </div>
            </div>
            <div class="row align-items-center my-2 even">
                <div class="col col-md-2">
                    <label for="call" class="col-form-label">稱謂</label>
                </div>
                <div class="col col-md-8">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="men" v-model="selected" value="1">
                        <label class="form-check-label" for="men">
                            先生
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="women" v-model="selected" value="2">
                        <label class="form-check-label" for="women">
                            女士
                        </label>
                    </div>
                </div>
            </div>
            <div class="row align-items-center my-2 odd">
                <div class="col col-md-2">
                    <label for="email" class="col-form-label">電子郵件</label>
                </div>
                <div class="col col-md-8">
                    <input type="email" id="email" class="form-control" required v-model="email">
                </div>
            </div>
            <div class="row align-items-center my-2 even">
                <div class="col col-md-2">
                    <label for="tel" class="col-form-label">聯絡電話</label>
                </div>
                <div class="col col-md-8">
                    <input type="text" id="tel" class="form-control" required v-model="tel">
                </div>
            </div>
            <div class="row align-items-center my-2 odd">
                <div class="col col-md-2">
                    <label for="comment" class="col-form-label">意見或需求描述</label>
                </div>
                <div class="col col-md-8">
                    <textarea type="text" class="form-control" rows="10" id="comment" required v-model="comment"></textarea>
                </div>
            </div>
            <div
                class="g-recaptcha my-2"
                data-sitekey="6LdhdkQkAAAAALbmTfFvfwnG_ob7H3lRa5wddxq8"
                data-theme="light" data-size="normal"
                data-callback="verify"
                data-expired-callback="expired"
                data-error-callback="error">
            </div>
            <div class="align-items-center">
                <div class="spinner-grow text-primary" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-secondary" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-success" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-danger" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-info" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-light" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-dark" role="status" v-if="show">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <button id="hiddenbutton" class="btn btn-primary d-none" @click="submit()" v-if="!show">送出</button>
            </div>            
        </div>
      </div>
    </div>
  </div>
</div>
<script>
     function verify() {
        document.getElementById("hiddenbutton").setAttribute("class", "btn btn-primary");
    }

    function expired() {
        document.getElementById("hiddenbutton").setAttribute("class", "btn btn-primary d-none");
        Swal.fire({
            title: 'Recapcha驗證過期',
            showConfirmButton: false,
            timer: 2000,
        })
        location.reload();
    }

    function error() {
        document.getElementById("hiddenbutton").setAttribute("class", "btn btn-primary d-none");
        Swal.fire({
            title: 'Recapcha驗證錯誤，請聯絡系統管理員',
            showConfirmButton: false,
            timer: 2000,
        })
    }

    var app = new Vue({
    el: '#app',
    data: {
        name:'',
        selected: 1,
        email: '',
        tel:'',
        comment: '',
        show: false,
    },
    methods: {
        async submit(){
            this.show = true;
            let submitComment = "";
            let submitCommentEmail = "";
            if(this.name == "" || this.email == "" || this.tel == "" || this.comment == ""){
                Swal.fire('所有欄位必須填寫。','','error')
            }else{
                submitComment = this.comment.replace(/\n/g,'<br/>')
                submitCommentEmail = this.comment.replace(/\n/g,'%0a')
                let postUrl = "/api/send-email/" + this.name + "&" + (this.selected == 1 ? "先生" : "女士") + "&" + this.email + "&" + this.tel + "&" + submitCommentEmail
                const response = await axios.get(postUrl);
                let formData = new FormData()
                formData.append('name', this.name);
                formData.append('title', this.selected == 1 ? "先生" : "女士");
                formData.append('email', this.email);
                formData.append('tel', this.tel);
                formData.append('comment', submitComment);
                const res = await axios.post('/api/contacts', formData)
                if (res.status === 200) {
                    Swal.fire('我們已接收到您的回覆，後續將由專人與您聯絡。','','info').then(function () { location.reload() })
                }else{
                    Swal.fire('系統錯誤，請聯繫系統管理人。','','error')
                }
            }
            this.show = false
        },
        
      },
    })
</script>
<style>
    .even {
        background-color: #fff4f4;
        border-bottom: 1px solid #ffffff;
    }
    .odd {
        background-color: #fbfbfb;
        border-bottom: 1px solid #ffffff;
    }
</style>
@endsection
