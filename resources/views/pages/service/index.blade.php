@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
      <h2 class="card-title fw-bold">專營項目</h2>
      <div class="text-center"><p>冷卻水系統水處理、冰水系統處理、水質監控設備及加藥設備、中央空調系統保養工程、減速機更新及維修、馬達更新及維修。</p></div>
      <!-- Carousel -->
      <div class="d-flex justify-content-center">
        <div id="demo" class="carousel slide" data-bs-ride="carousel">
            <!-- The slideshow/carousel -->
            <div class="carousel-inner rounded">
              <div class="carousel-item active">
                <img src="{{ asset('assets/A.jpg') }}" class="d-block w-100">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/B.jpg') }}" class="d-block w-100">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/C.jpg') }}" class="d-block w-100">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/D.jpg') }}" class="d-block w-100">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/E.jpg') }}" class="d-block w-100">
              </div>
            </div>

            <!-- Left and right controls/icons -->
            <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
              <span class="carousel-control-next-icon"></span>
            </button>
        </div>
      </div>

      <div class="d-flex justify-content-center mt-3">
        <div class="row" id="demo">
            <div class="col-md-6">
                <h5><b>關於保捷</b></h5>
                <p>
                  保捷開發實業有限公司成立於2018年，專營於冷卻系統的公司。

                  為使企業發展能夠多方面的延伸，也讓客戶於冷卻水系統能有專一窗口對應，於是展開了全方位的技術服務，先後成立了水處理事業部、  冷卻水塔零件維修事業部及空調系統清洗事業部。
                </p>
            </div>
            <div class="col-md-6">
                <h5><b>服務宗旨</b></h5>
                <p>
                  保捷開發多年來提供客戶多元的服務方式，已獲得客戶支持與肯定。未來保捷更會秉持以最「誠竭」、最『有效』的態度及效率來服務客戶，並提升客戶滿意度及優良口碑。
                </p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    var app = new Vue({
      el: '#app',
      data: {
          message: ''
      },
      mounted: function() {
          this.getData()
      },
      methods: {
        async getData(){
          const response = await axios.get("/api/homes/1");
          this.message = response.data.content;
        }
      },
    })
</script>
<style>
  #demo{
    width:90%;
  }
</style>
@endsection