@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
        <div class="container">
            <h1>網站地圖</h1>
            <ul>
                <li><a href="/">首頁</a></li>
            </ul>
            <ul>
                <li><a href="/">關於保捷</a></li>
            </ul>
            <ul>
                <li><a href="/WaterTreatment">事業部門</a></li>
                <ul>
                    <li><a href="/WaterTreatment">水處理事業部</a></li>
                    <li><a href="/CoolingTower">空調技術事業部</a></li>
                    <li><a href="/AirConditionTech">冷卻水塔維修事業部</a></li>
                    <li><a href="/WaterTreatmentSales">水處理銷售事業部</a></li>
                </ul>
            </ul>
            <ul>
                <li><a href="/Knowledge">知識庫</a></li>
            </ul>
            <ul>
                <li><a href="/Works">實績成果</a></li>
            </ul>
            <ul>
                <li><a href="/info">聯絡我們</a></li>
            </ul>
        </div>
    </div>
  </div>
</div>
@endsection