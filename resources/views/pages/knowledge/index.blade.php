@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
      <h2 class="card-title fw-bold">知識庫</h2>
      <div class="col col-md-4">
        
      </div>
      <div class="col col-md-8" v-html="message">
        
      </div>
    </div>
  </div>
</div>
<script>
    var app = new Vue({
      el: '#app',
      data: {
          message: ''
      },
      mounted: function() {
          this.getData()
      },
      methods: {
        async getData(){
          const response = await axios.get("/api/homes/6");
          this.message = response.data.content;
          console.log(response.data.content)
        }
      },
    })
</script>
@endsection