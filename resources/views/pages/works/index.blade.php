@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
      <h2 class="card-title fw-bold">實績成果</h2>
      <div class="col col-md-4">
        <ul class="mx-5">
            <li>
                <a class="nav-link link-dark" href="/Works">相片總覽</a>
            </li>
            <li>
                <a class="nav-link link-dark" href="/CompanyCooperating">實績客戶</a>
            </li>
        </ul>
      </div>
      <div class="col col-md-8" v-html="message" v-if="!carosel"></div>
      <div class="col col-md-8" v-if="false">
        <!-- Carousel -->
        <div id="demo" class="carousel slide" data-bs-ride="carousel">
          <!-- The slideshow/carousel -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{ asset('assets/1.JPG') }}" class="d-block w-100">
            </div>
            <div class="carousel-item" v-for="i in assets">
              <img :src="i" class="d-block w-100">
            </div>
          </div>

          <!-- Left and right controls/icons -->
          <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
          </button>
        </div>
      </div>

      <div class="col col-md-8" v-if="carosel">
        <div class="row">
          <div class="col-md-4 mt-2" v-for="asset in assets">
             <img id="img" :src="asset" onclick="onClick(this)">
          </div>
        </div>
      </div>

       <!-- Modal for full size images on click-->
      <div id="modal01" style="padding-top:0" onclick="this.style.display='none'">
          <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64" id="center-container">
              <img id="img01" class="w3-image" style="width: 50%;">
          </div>
      </div>

    </div>
  </div>
</div>
<script>
    // Modal Image Gallery
    function onClick(element) {
        document.getElementById("img01").src = element.src;
        document.getElementById("modal01").style.display = "block";
        //var captionText = document.getElementById("caption");
        //captionText.innerHTML = element.alt;
    }

    var app = new Vue({
      el: '#app',
      data: {
          message: '',
          carosel: false,
          assets: [],
      },
      mounted: function() {
          this.getData()
      },
      methods: {
        async getData(){
          let response = null;
          var pathname = window.location.pathname;
          this.assets = [];
          for(i = 2; i <= 81; i++){
            this.assets.push("assets/" + i + ".JPG")
          }
          switch (pathname){
            case '/Works' : {
              response = await axios.get("/api/homes/7");
              this.carosel = true;
              break;
            }
            case '/CompanyCooperating' : {
              response = await axios.get("/api/homes/7");
              this.message = response.data.content;
              break;
            }
            default: {
              break;
            }
          }
        },

      },
    })
</script>
<style>
#modal01 {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 10;
        background-color: rgba(0, 0, 0, 0.5);
    }
#center-container {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100%;
}

</style>
@endsection