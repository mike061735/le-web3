@extends('layouts.app')

@section('content')
<div id="app">
  <div class="card mt-4">
    <div class="card-body row">
      <h2 class="card-title fw-bold">事業部門</h2>
      <div class="col col-md-4">
        <ul class="mx-5">
            <li>
                <a class="nav-link link-dark" href="/WaterTreatment">水處理事業部</a>
            </li>
            <li>
                <a class="nav-link link-dark" href="/CoolingTower">冷卻水塔維修事業部</a>
            </li>
            <li>
                <a class="nav-link link-dark" href="/AirConditionTech">空調技術事業部</a>
            </li>
            <li>
                <a class="nav-link link-dark" href="/WaterTreatmentSales">水處理藥劑銷售事業部</a>
            </li>
        </ul>
      </div>
      <div class="col col-md-8" v-html="message">
        
      </div>
    </div>
  </div>
</div>
<script>
    var app = new Vue({
      el: '#app',
      data: {
          message: ''
      },
      mounted: function() {
          this.getData()
      },
      methods: {
        async getData(){
          let response = null;
          var pathname = window.location.pathname;
          switch (pathname){
            case '/WaterTreatment' : {
              response = await axios.get("/api/homes/2");
              this.message = response.data.content;
              break;
            }
            case '/CoolingTower' : {
              response = await axios.get("/api/homes/3");
              this.message = response.data.content;
              break;
            }
            case '/AirConditionTech' : {
              response = await axios.get("/api/homes/4");
              this.message = response.data.content;
              break;
            }
            case '/WaterTreatmentSales' : {
              response = await axios.get("/api/homes/5");
              this.message = response.data.content;
              break;
            }
            default: {
              break;
            }
          }
        }
      },
    })
</script>
@endsection