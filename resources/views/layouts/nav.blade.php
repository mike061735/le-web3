<nav class="navbar navbar-expand-lg navbar-light border-bottom border-danger">
  <div class="container-fluid">
    <a class="navbar-brand" href="/"><h2 class="fw-bold ml-5">保捷開發實業有限公司</h2></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" href="/about">關於保捷</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link active" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            事業部門
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item" href="/WaterTreatment">水處理事業部</a></li>
            <li><a class="dropdown-item" href="/CoolingTower">冷卻水塔維修事業部</a></li>
            <li><a class="dropdown-item" href="/AirConditionTech">空調技術事業部</a></li>
            <li><a class="dropdown-item" href="/WaterTreatmentSales">水處理藥劑銷售事業部</a></li>
          </ul>
        </li>        
        <li class="nav-item">
          <a class="nav-link active" href="/Knowledge">知識庫</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/Works">實績成果</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link active" href="/info" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            聯絡我們
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item" href="/service">需求服務</a></li>
            <li><a class="dropdown-item" href="/info">公司資訊</a></li>
          </ul>
        </li> 
      </ul>
    </div>
  </div>
</nav>
<style>
  nav li{
    font-size: 16px;
    padding: 0 10px;
  }
  nav li+li{
    border-left: 1px solid #6c757d;
  }
  .border-bottom{
    border-bottom: 3px solid #dc3545 !important
  }
  @media screen and (min-width: 1024px) {
    #navbarNavDropdown {
      display: flex;
      justify-content: end;
    }
  }
</style>