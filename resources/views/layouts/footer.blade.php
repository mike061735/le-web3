<footer class="py-3 my-4 border-top border-danger">
    <ul class="nav justify-content-end">
      <li class="nav-item"><a href="/" class="nav-link px-2 text-muted">首頁</a></li>
      <li class="nav-item"><a href="/about" class="nav-link px-2 text-muted">關於保捷</a></li>
      <li class="nav-item"><a href="/WaterTreatment" class="nav-link px-2 text-muted">事業部門</a></li>
      <li class="nav-item"><a href="/Knowledge" class="nav-link px-2 text-muted">知識庫</a></li>
      <li class="nav-item"><a href="/Works" class="nav-link px-2 text-muted">實績成果</a></li>
      <li class="nav-item"><a href="/info" class="nav-link px-2 text-muted">聯絡我們</a></li>
      <li class="nav-item"><a href="/SiteMap" class="nav-link px-2 text-muted">網站導覽</a></li>
    </ul>
    <ul class="nav justify-content-start">
      <li class="nav-item"><a href="mailto:baojie588@gmail.com" class="nav-link px-2 text-muted">雲林總公司-TEL:05-699-7095 E-mail: baojie588@gmail.com</a></li>
    </ul>
</footer>
<style>
  footer li{
    display: inline-block;
    font-size: 14px;
    padding: 0 10px;
  }
  footer li+li{
    border-left: 1px solid #6c757d;
  }
  .border-top{
    border-top: 3px solid #dc3545 !important
  }
</style>