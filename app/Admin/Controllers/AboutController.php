<?php

namespace App\Admin\Controllers;

use App\Models\Home;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AboutController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '頁面內容';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Home());
        $grid->disableCreation();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });

        $grid->column('id', __('Id'));
        $grid->column('title', __('標題'));
        //$grid->column('content', __('內容'));
        $grid->column('establish_date', __('建立日期'));
        $grid->column('created_at', __('建立於'));
        $grid->column('updated_at', __('更新於'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Home::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('標題'));
        $show->field('content', __('內容'));
        $show->field('establish_date', __('建立日期'));
        $show->field('created_at', __('建立於'));
        $show->field('updated_at', __('更新於'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Home());
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->text('title', __('標題'))->required();
        $form->ckeditor('content', __('內容'))->required();
        $form->datetime('establish_date', __('建立日期'))->required();

        return $form;
    }
}
