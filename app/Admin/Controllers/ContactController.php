<?php

namespace App\Admin\Controllers;

use App\Models\Contact;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ContactController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '聯絡我們-需求服務';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Contact());
        $grid->disableCreation();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });

        $grid->column('id', __('編號'));
        $grid->column('name', __('姓名'));
        $grid->column('title', __('稱呼'));
        $grid->column('email', __('Email'));
        $grid->column('tel', __('電話'));
        //$grid->column('comment', __('意見或需求描述'));
        $grid->column('created_at', __('建立於'));
        $grid->column('updated_at', __('更新於'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Contact::findOrFail($id));
        
        $show->field('id', __('編號'));
        $show->field('name', __('姓名'));
        $show->field('title', __('稱呼'));
        $show->field('email', __('Email'));
        $show->field('tel', __('電話'));
        $show->field('comment', __('意見或需求描述'));
        $show->field('created_at', __('建立於'));
        $show->field('updated_at', __('更新於'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Contact());
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->text('name', __('姓名'));
        $form->text('title', __('稱呼'));
        $form->email('email', __('Email'));
        $form->text('tel', __('電話'));
        $form->ckeditor('comment', __('意見或需求描述'));
        return $form;
    }
}
