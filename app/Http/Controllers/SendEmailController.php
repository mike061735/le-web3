<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyMail;

class SendEmailController extends Controller
{

    public function send($postdata)
    {
        error_log($postdata);
        $explodeStr = explode("&", $postdata);
        $from = ['email'=>'service.noreply.baojie@gmail.com', 'name'=>'保捷開發實業有限公司', 'subject'=>'通知您，有客戶於網站上提問！'];
        $to = ['email'=>'p0976923230@gmail.com'];
        $data = [   
                    'name'=>$explodeStr[0],
                    'title'=>$explodeStr[1],
                    'email'=>$explodeStr[2],
                    'tel'=>$explodeStr[3],
                    'comment'=>$explodeStr[4]
                ];
        if(Mail::send('mail.index', $data, function($message) use ($from, $to) {
            $message->from($from['email'], $from['name']);
            $message->to($to['email'])->cc(['madness1953@gmail.com','overwatchincome@gmail.com', 'p0905049771@gmail.com'])->subject($from['subject']);
        }))
        {
            return response()->json($data, 200);
        }else{
            return response()->json(["res"=>"error"], 500);
        }
        
        
    }
}
