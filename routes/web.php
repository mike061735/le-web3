<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('pages.service.index');
});
Route::get('/about', function () {
    return view('pages.home.index');
});
Route::get('/info', function () {
    return view('pages.contact.info');
});
Route::get('/service', function () {
    return view('pages.contact.service');
});
Route::get('/WaterTreatment', function () {
    return view('pages.department.index');
});
Route::get('/CoolingTower', function () {
    return view('pages.department.index');
});
Route::get('/AirConditionTech', function () {
    return view('pages.department.index');
});
Route::get('/WaterTreatmentSales', function () {
    return view('pages.department.index');
});
Route::get('/Knowledge', function () {
    return view('pages.knowledge.index');
});
Route::get('/Works', function () {
    return view('pages.works.index');
});
Route::get('/CompanyCooperating', function () {
    return view('pages.works.index');
});
Route::get('/SiteMap', function () {
    return view('pages.sitemap.index');
});
